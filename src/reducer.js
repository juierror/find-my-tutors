import firebase from "firebase";
// import { ENGINE_METHOD_CIPHERS } from "constants";

// init firebase database
var config = {
    apiKey: "AIzaSyAKlruKbJoSpEthq6ZQcxxHNcKA45PxbVQ",
    authDomain: "findmytutor-3f141.firebaseapp.com",
    databaseURL: "https://findmytutor-3f141.firebaseio.com",
    projectId: "findmytutor-3f141",
    storageBucket: "findmytutor-3f141.appspot.com",
    messagingSenderId: "511938470082"
};

firebase.initializeApp(config);

//initial state
var initstate = {
    isLogin: false,
    isStudent: true,
    allCourse: {},
    database: firebase.database(),
    searchResult: [],
    userValid: true,
    userid: "",
    tutorcourse: [],
    registerInformation: {},
    tutorrequest: {},
    studentregister: {}
}

// check storage
if (localStorage.id) {
    initstate.isLogin = true
    initstate.userid = localStorage.id
    if (localStorage.student == "false") {
        initstate.isStudent = false
    } else {
        initstate.isStudent = true
    }
}

// main reducer
export default (state = initstate, action) => {
    switch (action.type) {
        case "CHANGELOGIN":
            return {
                ...state,
                isLogin: action.isLogin
            }
        case "TOGGLELOGIN":
            return {
                ...state,
                isLogin: !state.isLogin
            }
        case "SETSTUDENT":
            if (state.isLogin == false) {
                return state
            } else {
                return {
                    ...state,
                    isStudent: true
                }
            }
        case "SETTUTOR":
            if (state.isLogin == false) {
                return state
            } else {
                return {
                    ...state,
                    isStudent: false
                }
            }
        case "UPDATECOURSE":
            // for (let a in action.allCourse) {
            //     console.log(a)
            // }
            return {
                ...state,
                allCourse: action.allCourse
            }
        case "UPDATESEARCHRESULT":
            let tmp_result = []
            //console.log(state.allCourse)
            // compute start and end price
            let start = action.price[0] * 10
            let end = (action.price[0] + action.price[1]) * 10

            // if student choose subject with None
            if (action.subject == 'None') {
                for (let k in state.allCourse) {
                    let price = state.allCourse[k].price
                    if (price >= start && price <= end) {
                        let tmp = state.allCourse[k]
                        tmp['courseid'] = k
                        tmp_result.push(tmp)
                    }
                }
                // if student choose subject
            } else {
                for (let k in state.allCourse) {
                    let price = state.allCourse[k].price
                    if (price >= start && price <= end && state.allCourse[k].subject == action.subject) {
                        let tmp = state.allCourse[k]
                        tmp['courseid'] = k
                        tmp_result.push(tmp)
                    }
                }
            }
            //console.log(tmp_result)
            return {
                ...state,
                searchResult: tmp_result
            }
        case "CHECKLOGIN":
            if (action.complete) {
                return {
                    ...state,
                    userid: action.id,
                    isLogin: true,
                    isStudent: action.student
                }
            } else {
                return {
                    ...state,
                    isLogin: false,
                    userValid: false
                }
            }
        case "TOGGLEUSERVALID":
            return {
                ...state,
                userValid: !state.userValid
            }
        case "LOGOUT":
            localStorage.removeItem("id")
            localStorage.removeItem("student")
            return {
                ...state,
                isLogin: false,
                userid: ""
            }
        case "STUDENTSIGNUP":
            state.database.ref("student/").once("value", (snap) => {
                if (!(action.iden in snap.val())) {
                    let iden = action.iden
                    let d = {
                        firstname: action.firstname,
                        lastname: action.lastname,
                        gender: action.gender,
                        birthyear: action.birthyear,
                        study: action.study
                    }
                    state.database.ref(`student/${iden}`).set(d)
                    // console.log({
                    //     id: action.iden,
                    //     password: action.password,
                    //     studentstate: 1
                    // })
                    state.database.ref(`account/${action.username}`).set({
                        id: action.iden,
                        password: action.password,
                        studentstate: 1
                    })
                }
            })
            return state
        case "TUTORSIGNUP":
            state.database.ref("student/").once("value", (snap) => {
                if (!(action.iden in snap.val())) {
                    let ident = action.iden
                    let dt = {
                        firstname: action.firstname,
                        lastname: action.lastname,
                        gender: action.gender,
                        birthyear: action.birthyear,
                        experience: action.experience,
                        bank: action.bank,
                        banknumber: action.banknumber,
                        blackliststate: 0,
                        email: action.email,
                        phonenumber: action.phonenumber
                    }
                    state.database.ref(`tutor/${ident}`).set(dt)
                    state.database.ref(`account/${action.username}`).set({
                        id: ident,
                        password: action.password,
                        studentstate: 0
                    })
                }
            })
            return state
        case "STUDENTEDIT":
            let dse = {
                firstname: action.firstname,
                lastname: action.lastname,
                gender: action.gender,
                birthyear: action.birthyear,
                study: action.study
            }
            state.database.ref(`student/${state.userid}`).update(dse)
            return state
        case "TUTOREDIT":
            let dte = {
                firstname: action.firstname,
                lastname: action.lastname,
                gender: action.gender,
                birthyear: action.birthyear,
                experience: action.experience,
                bank: action.bank,
                banknumber: action.banknumber,
                blackliststate: 0,
                email: action.email,
                phonenumber: action.phonenumber
            }
            state.database.ref(`tutor/${state.userid}`).update(dte)
            return state
        case "GETCOURSE":
            let idgc = action.iden
            let mycourseresult = []
            // for (let k in state.allCourse) {
            //     let cid = state.allCourse[k]
            //     if (cid.startsWith(idgc)) {
            //         let tmp = state.allCourse[k]
            //         tmp['courseid'] = k
            //         mycourseresult.push(tmp)
            //     }
            // }
            for (let c in state.allCourse) {
                if (c.startsWith(idgc)) {
                    let tmp = state.allCourse[c]
                    tmp['courseid'] = c
                    mycourseresult.push(tmp)
                }
            }
            return {
                ...state,
                tutorcourse: mycourseresult
            }
        case "GETCOURSEINFO":
            // console.log("get info")
            if (action.courseid in state.allCourse) {
                return {
                    ...state,
                    registerInformation: state.allCourse[action.courseid]
                }
            }
            return state
        case "DELETEACCOUNT":
            let tmp_userid = state.userid
            state.database.ref("account/").once("value", (snap) => {
                for (let accountname in snap.val()) {
                    if (snap.val()[accountname].id == tmp_userid) {
                        state.database.ref(`account/${accountname}`).remove()
                        break
                    }
                }
                if (state.isStudent) {
                    state.database.ref(`student/${tmp_userid}`).remove()
                } else {
                    state.database.ref(`tutor/${tmp_userid}`).remove()
                }
                localStorage.removeItem("id")
                localStorage.removeItem("student")
            })
            return {
                ...state,
                isLogin: false,
                userid: ""
            }
        case "CREATECOURSE":
            //find max course max id
            let max_course_id = -1
            for (let c in state.allCourse) {
                if (c.startsWith(state.userid)) {
                    max_course_id = parseInt(c) > max_course_id ? parseInt(c) : max_course_id
                }
            }
            let new_course = {
                address: action.address,
                subject: action.subject,
                price: action.price,
                time: action.time,
                coursename: action.coursename
            }
            if (max_course_id < 0) {
                // padding 00
                max_course_id = parseInt(state.userid) * 100
            }
            state.database.ref(`course/${max_course_id + 1}`).set(new_course)
            return state
        case "UPDATESTUDENTREGISTER":
            return {
                ...state,
                studentregister: action.studentregister
            }
        case "UPDATETUTORREQUEST":
            return {
                ...state,
                tutorrequest: action.tutorrequest
            }
        case "REGISTERCOURSE":
            state.database.ref(`studentregister/${state.userid}/${action.courseid}`).set({
                state: 0,
                address: action.address,
                time: action.time,
                image: "None"
            })
            state.database.ref(`tutorrequest/${action.courseid}/${state.userid}`).set({
                state: 0,
                address: action.address,
                time: action.time,
                image: "None"
            })
            return state
        case "CANCELCOURSE":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).remove()
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).remove()
            return state
        case "TUTORREJECT":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).remove()
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).update({
                state: 1
            })
            return state
        case "TUTORACCEPT":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).update({
                state: 2
            })
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).update({
                state: 2
            })
            return state
        case "STUDENTPAYMENT":
            state.database.ref(`tutorrequest/${action.courseid}/${state.userid}`).update({
                image: action.image,
                state: 3
            })
            state.database.ref(`studentregister/${state.userid}/${action.courseid}`).update({
                image: action.image,
                state: 3
            })
            return state
        case "TUTORCONFIRMPAYMENT":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).update({
                state: 4
            })
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).update({
                state: 4
            })
            return state
        case "TUTORNOTCONFIRMPAYMENT":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).update({
                state: 5
            })
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).update({
                state: 5
            })
            return state
        case "TUTORALREADYTEACH":
            state.database.ref(`tutorrequest/${action.courseid}/${action.studentid}`).update({
                state: 6
            })
            state.database.ref(`studentregister/${action.studentid}/${action.courseid}`).update({
                state: 6
            })
            return state
        case "STUDENTCLOSE":
            state.database.ref(`tutorrequest/${action.courseid}/${state.userid}`).remove()
            state.database.ref(`studentregister/${state.userid}/${action.courseid}`).remove()
            return state
        default:
            return state
    }
}


// action
export const changeLogin = (b) => ({
    type: "CHANGELOGIN",
    isLogin: b
})

export const toggleLogin = () => ({
    type: "TOGGLELOGIN"
})

export const setStudent = () => ({
    type: "SETSTUDENT"
})

export const setTutor = () => ({
    type: "SETTUTOR"
})

export const updateCourse = (courseOb) => ({
    type: "UPDATECOURSE",
    allCourse: courseOb
})

export const updateSearchResult = (p, s) => ({
    type: "UPDATESEARCHRESULT",
    price: p,
    subject: s
})

export const checkLogin = (com, s, i) => ({
    type: "CHECKLOGIN",
    complete: com,
    student: s,
    id: i
})

export const toggleUserValid = () => ({
    type: "TOGGLEUSERVALID"
})

export const logout = () => ({
    type: "LOGOUT"
})

export const studentSignup = (id, firstName, lastName, g, birthYear, stu, user, pass) => ({
    type: "STUDENTSIGNUP",
    iden: id,
    firstname: firstName,
    lastname: lastName,
    gender: g,
    birthyear: birthYear,
    study: stu,
    username: user,
    password: pass
})

export const tutorSignup = (id, firstName, lastName, g, birthYear, ex, ba, banknum, em, phonenum, user, pass) => ({
    type: "TUTORSIGNUP",
    iden: id,
    firstname: firstName,
    lastname: lastName,
    gender: g,
    birthyear: birthYear,
    experience: ex,
    bank: ba,
    banknumber: banknum,
    email: em,
    phonenumber: phonenum,
    username: user,
    password: pass
})

export const studentEdit = (firstName, lastName, g, birthYear, stu) => ({
    type: "STUDENTEDIT",
    firstname: firstName,
    lastname: lastName,
    gender: g,
    birthyear: birthYear,
    study: stu
})

export const tutorEdit = (firstName, lastName, g, birthYear, ex, ba, banknum, em, phonenum) => ({
    type: "TUTOREDIT",
    firstname: firstName,
    lastname: lastName,
    gender: g,
    birthyear: birthYear,
    experience: ex,
    bank: ba,
    banknumber: banknum,
    email: em,
    phonenumber: phonenum
})

export const getcourse = (tutorid) => ({
    type: "GETCOURSE",
    iden: tutorid
})

export const getCourseInfo = (cid) => ({
    type: "GETCOURSEINFO",
    courseid: cid
})

export const getTutorInfo = (tid) => ({
    type: "GETTUTORINFO",
    tutorid: tid
})

export const deleteAccount = () => ({
    type: "DELETEACCOUNT"
})

export const createCourse = (ad, name, pr, sub, tim) => ({
    type: "CREATECOURSE",
    address: ad,
    coursename: name,
    price: pr,
    subject: sub,
    time: tim

})

export const updateTutorRequest = (obj) => ({
    type: "UPDATETUTORREQUEST",
    tutorrequest: obj
})

export const updateStudentRegister = (obj) => ({
    type: "UPDATESTUDENTREGISTER",
    studentregister: obj
})


/* REGISTER LOGIC */
// state 0
export const registerCourse = (id, add, tim) => ({
    type: "REGISTERCOURSE",
    courseid: id,
    address: add,
    time: tim
})
// delete student and tutor request
export const cancelCourse = (id, stid) => ({
    type: "CANCELCOURSE",
    courseid: id,
    studentid: stid
})
// state 1, delete request in tutor box
export const tutorReject = (id, stid) => ({
    type: "TUTORREJECT",
    courseid: id,
    studentid: stid
})
// state 2
export const tutorAccept = (id, stid) => ({
    type: "TUTORACCEPT",
    courseid: id,
    studentid: stid
})
// state 3
export const studentPayment = (id, img) => ({
    type: "STUDENTPAYMENT",
    courseid: id,
    image: img
})
// state 4
export const tutorConfirmPayment = (id, stid) => ({
    type: "TUTORCONFIRMPAYMENT",
    courseid: id,
    studentid: stid
})
// state 5
export const tutorNotConfirmPayment = (id, stid) => ({
    type: "TUTORNOTCONFIRMPAYMENT",
    courseid: id,
    studentid: stid
})
// state 6
export const tutorAlreadyTeach = (id, stid) => ({
    type: "TUTORALREADYTEACH",
    courseid: id,
    studentid: stid
})
// state 7
export const studentClose = (id) => ({
    type: "STUDENTCLOSE",
    courseid: id,
})
/* CLOSE REGISTER LOGIC */