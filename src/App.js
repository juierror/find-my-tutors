import React, { Component } from "react";
//import logo from "./logo.svg";
import "./App.css";
import Login from "./Component/Login";
import NotLogin from "./Component/NotLogin";
import { updateCourse, getcourse, updateTutorRequest, updateStudentRegister } from './reducer'

import { connect } from 'react-redux'
import 'tachyons';

class App extends Component {
  constructor() {
    super()
  }
  componentDidMount() {
    // console.log(this.props.database)
    this.refer = this.props.database.ref('course/')
    this.refer.on("value", (snap) => {
      //console.log(snap.val())
      this.props.updateCourse(snap.val())
      if (this.props.isLogin && !this.props.isStudent) {
        this.props.getcourse(this.props.userid)
      }
    })

    this.props.database.ref(`studentregister/`).once("value", snap => {
      //console.log(snap.val())
      if (snap.val() != null) this.props.updateStudentRegister(snap.val())
      // this.props.updateStudentRegister(snap.val())
    })

    this.props.database.ref(`tutorrequest/`).once("value", snap => {
      if (snap.val() != null) this.props.updateTutorRequest(snap.val())
    })
  }
  render() {
    return (
      <div className="App">
        {this.props.isLogin ? <Login /> : <NotLogin />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLogin: state.isLogin,
  database: state.database,
  isStudent: state.isStudent,
  userid: state.userid
})

const mapDispatchToProps = {
  updateCourse,
  getcourse,
  updateTutorRequest,
  updateStudentRegister
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
