import React, { Component } from "react";
import { connect } from 'react-redux';
import MultiSlider from "multi-slider";
import { updateSearchResult } from './../reducer'
import { NavLink } from "react-router-dom";
import './Search.css';
class Search extends Component {
	constructor() {
		super();
		this.state = {
			price_tag: [0, 100, 0],
			subject_tag: "None",
			subject_list: [
				"None",
				"Computer",
				"Physics",
				"Math",
				"Chemistry",
				"Science",
				"Biology",
				"Thai",
				"Social Studies",
				"English",
				"Japan",
				"German",
				"Russia",
				"Other"
			],
		};
	}

	render() {
		return (
			<div className="search-page">
				{/*search-container[begin]--------------------------------------------------*/}
				<div className='search-filter-container'>
					<h5 className='search-filter-text'><b>Filter</b></h5>

					<div className='price-filter'>
						<p className='search-filter-text'>Price Range</p>
						<MultiSlider colors={["#33cc33", "#29a329", "#33cc33"]} values={this.state.price_tag} onChange={(v) => {
							this.setState({
								price_tag: v,
							});
						}} />
						<p>Price {this.state.price_tag[0] * 10} - {(this.state.price_tag[0] + this.state.price_tag[1]) * 10} THB</p>
					</div>
					<hr />
					<p className='search-filter-text'>Subject</p>

					<div className='search-row'>
						<div className='search-select'>
							<select
								class="custom-select mr-sm-2"
								id="inlineFormCustomSelect"
								value={this.state.subject_tag}
								name="subject"
								onChange={e => {
									this.setState({ subject_tag: e.target.value });
								}}
							>
								{this.state.subject_list.map((val, idx) => {
									return <option value={val}>{val}</option>;
								})}
							</select>
						</div>
						<div className='search-select'>
							<button
								type="button"
								class="btn btn-primary"
								onClick={(e) => {
									this.props.updateSearchResult(this.state.price_tag, this.state.subject_tag)
								}}>
								Search
							</button>
						</div>
					</div>
				</div>
				{/*search-container[end]----------------------------*/}
				<div className='search-result-container'>
					{this.props.searchResult.map((val, idx) => {
						return (createCard(val, idx));
					}
					)}
				</div>

				<div className='go-top'>
					<button
						id='topBtn'
						type="button"
						class="btn btn-primary"
						onClick={(e) => {
							window.scrollTo(0, 0);
						}}>
						Top
			</button>
				</div>

			</div>
		);
	}
}

function createCard(val, idx) {
	return (
		<div className="card-body">
			<h5 className="card-title">{val.coursename}</h5>
			<h6 className="card-subtitle mb-2 text-muted"> Subject : </h6>
			<h6 className="card-text">{val.subject}</h6>
			<br />
			<h6 className="card-subtitle mb-2 text-muted"> Price : </h6>
			<h6 className="card-text">{val.price}</h6>
			<br />
			<h6 className="card-subtitle mb-2 text-muted"> Address : </h6>
			<h6 className="card-text">{val.address}</h6>
			<br />
			<h6 className="card-subtitle mb-2 text-muted"> Time : </h6>
			<h6 className="card-text">{val.time}</h6>
			<br />
			<h6 className="card-subtitle mb-2 text-muted"> Course id : </h6>
			<h6 className="card-text">{val.courseid}</h6>
			<br />
			<a className="card-link">
				<NavLink to={{ pathname: "/register", search: `?id=${val.courseid}` }}>Register</NavLink>
			</a>
			<a className="card-link">
				<NavLink to={{ pathname: "/course", search: `?id=${val.courseid}` }}>See More</NavLink>
			</a>
		</div>
	);
}

const mapStateToProps = state => ({
	allCourse: state.allCourse,
	searchResult: state.searchResult
})

const mapDispatchToProps = {
	// add name of action from reducer (don't forget to import)
	updateSearchResult //when click serach it will update searchResult in store
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);
