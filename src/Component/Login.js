import React, { Component } from "react";
import { connect } from 'react-redux'
import Student from './Student'
import Tutor from './Tutor'
import { setStudent, setTutor } from './../reducer'

class Login extends Component {
    constructor() {
        super()
    }
    render() {
        return (
            <div>
                {this.props.isStudent ? <Student /> : <Tutor />}
            </div>
        );
    }
}


const mapStateToProps = state => ({
    isStudent : state.isStudent
})

const mapDispatchToProps = {
    setStudent,
    setTutor
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
