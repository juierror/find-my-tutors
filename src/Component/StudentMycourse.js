import React, { Component } from "react";
import { connect } from 'react-redux'
import { cancelCourse, studentPayment, studentClose, updateStudentRegister, updateTutorRequest } from './../reducer'
import { NavLink } from "react-router-dom";
import "./studentMyCourse.css";
class StudentMycourse extends Component {
    constructor() {
        super()
        this.state = {
            register: {}
        }
    }

    componentDidMount() {
        this.setState({ register: this.props.studentregister[this.props.userid] || {} })
    }
    stateToOb(state, courseid) {
        state = parseInt(state)
        if (state == 2) {
            return (
                <div>
                    <h6 style={{ color: "#8899a6" }}>Tutor Already Accept your request, please pay 30% of course price</h6>
                    <h6>You must provide evidence image link of payment.</h6>
                    <button
                        className="btn btn-outline-success"
                        onClick={e => {
                            let prom = window.prompt("Link of your evidence image of payment.")
                            if (prom !== null && prom !== "") {
                                this.props.studentPayment(courseid, prom)
                            }
                            this.props.database.ref(`tutorrequest/`).once("value", snap => {
                                console.log(snap.val());
                                if (snap.val() != null) this.props.updateTutorRequest(snap.val());
                            })
                        }
                        }>
                        Attach Image
                        </button>
                    <button
                        className="btn btn-outline-danger btn-profile"
                        onClick={e => {
                            if (window.confirm("you sure to cancle this course?")) {
                                this.props.cancelCourse(courseid, this.props.userid)
                                this.props.database.ref(`studentregister/`).once("value", snap => {
                                    console.log(snap.val())
                                    if (snap.val() != null) this.props.updateStudentRegister(snap.val())
                                })
                            }

                        }}>
                        Cancel Course
                    </button>
                </div>
            )
        } else if (state == 1) {
            return (
                <div>
                    <h6>Your request have been rejected.</h6>
                    <button
                        className="btn btn-outline-danger"
                        onClick={e => {
                            if (window.confirm("you sure to cancle this course?")) {
                                this.props.cancelCourse(courseid, this.props.userid)
                                this.props.database.ref(`studentregister/`).once("value", snap => {
                                    console.log(snap.val())
                                    if (snap.val() != null) this.props.updateStudentRegister(snap.val())
                                })
                            }
                        }}>
                        Cancel Course
                </button>
                </div>
            )

        } else if (state == 6) {
            return (
                <div>
                    <h6>Tutor Already Teach</h6>
                    <button
                        className="btn btn-outline-success"
                        onClick={e => {
                            this.props.studentClose(courseid);
                            this.props.database.ref(`tutorrequest/`).once("value", snap => {
                                console.log(snap.val());
                                if (snap.val() != null) this.props.updateTutorRequest(snap.val());
                            })
                        }
                        }>Accept to Close Course</button>
                </div>
            )
        } else if (state == 5) {
            return (
                <div>
                    <h6>Tutor Not Confirm Your Payment.</h6>
                    <button
                        className="btn btn-outline-success"
                        onClick={e => {
                            let prom = window.prompt("Link of your evidence image of payment.")
                            if (prom !== null && prom !== "") {
                                this.props.studentPayment(courseid, prom)
                            }
                            this.props.database.ref(`tutorrequest/`).once("value", snap => {
                                console.log(snap.val());
                                if (snap.val() != null) this.props.updateTutorRequest(snap.val());
                            })
                        }
                        }>
                        Attach Image Again
                        </button>
                    <button
                        className="btn btn-outline-danger btn-profile"
                        onClick={e => {
                            if (window.confirm("you sure to cancle this course?")) {
                                this.props.cancelCourse(courseid, this.props.userid)
                                this.props.database.ref(`studentregister/`).once("value", snap => {
                                    console.log(snap.val())
                                    if (snap.val() != null) this.props.updateStudentRegister(snap.val())
                                })
                            }

                        }}>
                        Cancel Course
                    </button>
                </div>
            )
        } else if (state == 4) {
            return <h6 style={{ color: "#8899a6" }}>Confirmed, Waiting to study </h6>
        }
        return (
            <div>
                <button
                    className="btn btn-outline-danger btn-profile"
                    onClick={e => {
                        if (window.confirm("you sure to cancle this course?")) {
                            this.props.cancelCourse(courseid, this.props.userid)
                            this.props.database.ref(`studentregister/`).once("value", snap => {
                                console.log(snap.val())
                                if (snap.val() != null) this.props.updateStudentRegister(snap.val())
                            })
                        }
                    }}>
                    Cancel Course
                </button>
            </div>
        )
    }

    render() {
        return (
            <div className="background01">
                <div className="studentCourse-page">
                    <div className="course-contain">
                        <div className="titleOnBorder">
                            <b>Course status</b>
                        </div>
                        {Object.keys(this.state.register).map((courseid, idx) => {
                                return (
                                    <div className="card-body">
                                        <NavLink to={{ pathname: "/course", search: `?id=${courseid}` }} style={{ color: "#212529" }}>
                                        <h4 className="studentcard-title">{this.props.allCourse[courseid].coursename}</h4> 
                                        </NavLink>
                                        <h6 className="card-title">Address: {this.state.register[courseid].address}</h6>
                                        <h6 className="card-title">Time: {this.state.register[courseid].time}</h6>
                                        {/* ToDelete */}
                                        <h6 className="card-title">state: {this.state.register[courseid].state}</h6>
                                        {/* ToDelete */}
                                        {this.stateToOb(this.state.register[courseid].state, courseid)}

                                    </div>
                                )
                            })
                        }
                    </div>                
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    registerInformation: state.registerInformation,
    userid: state.userid,
    studentregister: state.studentregister,
    database: state.database,
    allCourse: state.allCourse
})

const mapDispatchToProps = {
    cancelCourse,
    //state 3
    studentPayment,
    studentClose,
    updateStudentRegister,
    updateTutorRequest
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentMycourse);
