import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { logout } from "./../reducer"
import { connect } from "react-redux"

class StudentHeader extends Component {
    constructor() {
        super()
    }
    render() {
        return (
            <div className="studentHeader">
                <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "rgb(4, 155, 229)", padding: "0px" }}>
                    <NavLink className="juiheaderbutton" exact to="/" activeClassName="active" style={{ textDecoration: 'none' }}><i class="fas fa-home"></i> Find My Tutors</NavLink>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent" >
                        <ul className="navbar-nav mr-auto juiheader"  >

                            <li><NavLink className="juiheaderbutton" to="/search/" activeClassName="active" style={{ textDecoration: 'none' }}><i class="fas fa-search"></i> Search</NavLink></li>
                            <li><NavLink className="juiheaderbutton" to="/studentcourse/" activeClassName="active" style={{ textDecoration: 'none' }}><i class="fas fa-book"></i> My Course</NavLink></li>

                        </ul>
                        <ul className="navbar-nav ml-auto juiheader" >
                            <li><NavLink className="juiheaderbutton" to="/profile/" activeClassName="active" style={{ textDecoration: 'none' }}><i class="fas fa-user"></i> Profile</NavLink></li>
                            <li><NavLink className="juiheaderbutton" to="/" activeClassName="active" style={{ textDecoration: 'none' }}><div onClick={e => { this.props.logout() }}><i class="fas fa-sign-out-alt"></i> Logout</div></NavLink></li>
                        </ul>

                    </div>

                </nav>
            </div>

        );
    }
}
const mapStateToProps = state => ({
})

const mapDispatchToProps = {
    logout
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentHeader);