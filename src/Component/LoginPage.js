import React, { Component } from "react";
import { connect } from 'react-redux'
import { toggleLogin, checkLogin, toggleUserValid, getcourse } from './../reducer'

class LoginPage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			user: "",
			password: ""
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	handleSubmit(event) {
		// alert('Submit');
		event.preventDefault();

		this.props.database.ref("account/").once("value", (snap) => {
			let account = snap.val()
			let complete = false
			let student = true
			let id = ""
			if (this.state.user in account && this.state.password == account[this.state.user]["password"]) {
				if (account[this.state.user]["studentstate"] == 0) {
					student = false
				}
				localStorage.id = account[this.state.user]["id"]
				localStorage.student = student
				complete = true
				id = account[this.state.user]["id"]
				console.log("login complete")
				this.props.checkLogin(complete, student, id)
				if (!this.props.userValid) {
					if (window.confirm("Username/Password not valid")) {
						this.props.toggleUserValid()
					}
				}
			} else {
				this.props.toggleUserValid()
			}

			if (!this.props.userValid) {
				if (window.confirm("Username/Password not valid")) {
					this.props.toggleUserValid()
				}
			} else {
				this.props.getcourse(id)
			}
		})

		// setTimeout(() => {
		// 	if (!this.props.userValid) {
		// 		if (window.confirm("Username/Password not valid")) {
		// 			this.props.toggleUserValid()
		// 		}
		// 	}
		// }, 3000)
	}

	render() {
		return (
			<div className="login-block">
				<h1>Login</h1>
				<form onSubmit={this.handleSubmit} className="login-form" style={{ borderRadius: "32px" }}>
					<label>
						Username:
          				<input type="text" id="user" value={this.state.user} onChange={this.handleChange} />
					</label>
					<label>
						Password:
          				<input type="password" id="password" value={this.state.password} onChange={this.handleChange} />
					</label>
					<button type="submit">login</button>
				</form>
			</div>
		)
	}
}
const mapStateToProps = state => ({
	isLogin: state.isLogin,
	userValid: state.userValid,
	database: state.database
})

const mapDispatchToProps = {
	toggleLogin,
	checkLogin,
	toggleUserValid,
	getcourse
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

