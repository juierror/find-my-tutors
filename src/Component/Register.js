import React, { Component } from "react";
import { connect } from 'react-redux'
import { getCourseInfo, registerCourse, updateStudentRegister, updateTutorRequest } from './../reducer'
import './Register.css';

class Register extends Component {
    constructor() {
        super()
        this.state = {
            address: "",
            time: "",
            courseid: ""
        }
    }

    handleSubmit = (e) => {
        if (window.confirm('confirm to submit?')) {
            this.props.registerCourse(this.state.courseid, this.state.address, this.state.time)
            this.props.database.ref(`tutorrequest/`).once("value", snap => {
                console.log(snap.val())
                if (snap.val() != null) this.props.updateTutorRequest(snap.val())
            })
            this.props.database.ref(`studentregister/`).once("value", snap => {
                console.log(snap.val())
                if (snap.val() != null) this.props.updateStudentRegister(snap.val())
            })
        }
        e.preventDefault();
    }
    componentDidMount() {
        let params = new URLSearchParams(this.props.location.search);
        this.setState({
            courseid: params.get("id")
        })
        this.props.getCourseInfo(params.get("id"))
    }
    render() {
        return (
            <div class="container_register">
                <h1>Register {this.state.courseid}</h1>
                <form onSubmit={(e) => { this.handleSubmit(); return false; }} action="#">
                    <div class="new_row">
                        <div class="col-25">
                            <label class="label_register">Course Name: </label>
                        </div>
                        <div class="col-75">
                            <label class="">{this.props.registerInformation["coursename"]}</label>
                        </div>

                    </div>
                    <div class="new_row">
                        <div class="col-25">
                            <label class="label_register">Subject:</label>
                        </div>
                        <div class="col-75">
                            <label class="">{this.props.registerInformation["subject"]}</label>
                        </div>
                    </div>
                    <div class="new_row">
                        <div class="col-25">
                            <label class="label_register">Original Address:</label>
                        </div>
                        <div class="col-75">
                            <label class="">{this.props.registerInformation["address"]}</label>
                        </div>
                    </div>
                    <div class="new_row">
                        <div class="col-25">
                            <label class="">Address</label>
                        </div>
                        <div class="col-75">
                            <input type="text" class=" input_text" placeholder="address" value={this.state.address} onChange={e => {
                                this.setState({ address: e.target.value })
                            }} />
                        </div>
                    </div>
                    <div class="new_row">
                        <div class="col-25">
                            <label class="label_register">Original Time:</label>
                        </div>
                        <div class="col-75">
                            <label class="">{this.props.registerInformation["time"]}</label>
                        </div>
                    </div>
                    <div class="new_row">
                        <div class="col-25"> 
                            <label class="label_register">Time</label>
                        </div>
                        <div class="col-75">
                            <input type="text" class="input_text" placeholder="time" value={this.state.time} onChange={e => {
                                this.setState({ time: e.target.value })
                            }} />
                        </div>
                    </div>
                    <div class="new_row">
                        <div class="col-25">
                            <label class="label_register">Price:</label>
                        </div>
                        <div class="col-75">
                            <label class="">{this.props.registerInformation["price"]}</label>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-outline-primary">Confirm</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    registerInformation: state.registerInformation,
    userid: state.userid
})

const mapDispatchToProps = {
    getCourseInfo,
    registerCourse,
    updateTutorRequest,
    updateStudentRegister
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);
