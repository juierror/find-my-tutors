import React, { Component } from "react";
import { getCourseInfo } from './../reducer'
import { connect } from 'react-redux'
import { NavLink } from "react-router-dom";
import './Course.css';

class Course extends Component {
    constructor() {
        super()
        this.state = {
            courseid: "",
            tutorInfo: {}
        }
    }

    componentDidMount() {
        let params = new URLSearchParams(this.props.location.search);
        this.setState({
            courseid: params.get("id")
        })
        this.props.getCourseInfo(params.get("id"))
        let tid = params.get("id").substring(0, 10)
        this.props.database.ref(`tutor/${tid}`).once("value", (snap) => {
            this.setState({
                tutorInfo: snap.val()
            })
        })
    }

    render() {
        return (
            <div className="course-page">
                <br/>
                {/*<h1>Courses {this.state.courseid}</h1>*/}
                <div className='course-info'>
                    <h3>Course Info</h3>
                    <h5 className='course-subtitle mb-2 text-muted'>Course Name: </h5>
                    <h5 className='course-text'>{this.props.registerInformation["coursename"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Subject: </h5>
                    <h5 className='course-text'>{this.props.registerInformation["subject"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Address: </h5>
                    <h5 className='course-text'>{this.props.registerInformation["address"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Time: </h5>
                    <h5 className='course-text'>{this.props.registerInformation["time"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Price: </h5>
                    <h5 className='course-text'>{this.props.registerInformation["price"]}</h5><br/>
                </div>
                <br/>
                <div className='tutor-info'>
                    <h3>Tutor Info</h3>
                    <h5 className='course-subtitle mb-2 text-muted'>First Name: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["firstname"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Last Name: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["lastname"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Gender: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["gender"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Birth Year: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["birthyear"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>email: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["email"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Phone Number: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["phonenumber"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Bank: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["bank"]}</h5><br/>
                    <h5 className='course-subtitle mb-2 text-muted'>Bank Number: </h5>
                    <h5 className='course-text'>{this.state.tutorInfo["banknumber"]}</h5><br/>
                </div>
                <br/>
                <div className='tutor-exp'>
                    <h5>Experience: {this.state.tutorInfo["experience"]}</h5>
                </div>
                <br/>
                {
                        this.props.isStudent && <button className="btn btn-outline-primary"><NavLink to={{ pathname: "/register", search: `?id=${this.state.courseid}` }}>Register</NavLink></button>
                }
            </div>
            
        )
    }
}

const mapStateToProps = state => ({
    registerInformation: state.registerInformation,
    database: state.database,
    isStudent: state.isStudent
})

const mapDispatchToProps = {
    getCourseInfo
}
export default connect(mapStateToProps, mapDispatchToProps)(Course);
