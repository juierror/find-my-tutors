import React, { Component } from "react";
import "./profile.css";
import "./Search.css";
import { studentEdit, tutorEdit, getcourse, deleteAccount } from "./../reducer";
import { connect } from "react-redux";

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      information: {},
      isEdit: false,
      firstname: "",
      lastname: "",
      gender: "male",
      birthyear: "",
      email: "",
      bank: "",
      banknumber: "",
      phonenumber: "",
      studyexp: "",
      exp: ""
    };
  }
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  clickBack = () => {
    this.setState({
      isEdit: false
    });
  };

  clickEdit = () => {
    this.setState({
      isEdit: true
    });
  };

  clickDeleteAccount = () => {
    if (window.confirm("Are you sure to delete this account?")) {
      this.props.deleteAccount();
    }
  };

  clickSave = () => {
    if (window.confirm("Are you sure to edit this account information?")) {
      if (this.props.isStudent) {
        this.props.studentEdit(
          this.state.firstname,
          this.state.lastname,
          this.state.gender,
          this.state.birthyear,
          this.state.studyexp
        );
      } else {
        this.props.tutorEdit(
          this.state.firstname,
          this.state.lastname,
          this.state.gender,
          this.state.birthyear,
          this.state.exp,
          this.state.bank,
          this.state.banknumber,
          this.state.email,
          this.state.phonenumber
        );
      }
    }
    this.setState({
      isEdit: false
    });
  };

  componentDidMount() {
    if (this.props.isStudent) {
      this.props.database
        .ref(`student/${this.props.userid}`)
        .on("value", snap => {
          // haapy coding on valentine's day
          if (snap.val() !== null) {
            this.setState({
              information: snap.val(),
              firstname: snap.val()["firstname"] || "",
              lastname: snap.val()["lastname"] || "",
              gender: snap.val()["gender"] || "male",
              studyexp: snap.val()["study"] || "",
              birthyear: snap.val()["birthyear"] || ""
            });
          }
        });
    } else {
      this.props.database
        .ref(`tutor/${this.props.userid}`)
        .on("value", snap => {
          if (snap.val() !== null) {
            this.setState({
              information: snap.val(),
              firstname: snap.val()["firstname"] || "",
              lastname: snap.val()["lastname"] || "",
              gender: snap.val()["gender"] || "male",
              exp: snap.val()["experience"] || "",
              birthyear: snap.val()["birthyear"] || "",
              bank: snap.val()["bank"] || "",
              banknumber: snap.val()["banknumber"] || "",
              email: snap.val()["email"] || "",
              phonenumber: snap.val()["phonenumber"] || ""
            });
          }
        });
    }
  }
  render() {
    return (
      <div className="background01">
        <div className="profile-page">
          <div className="profileContainer">
            <div className="imgBox">
              <img
                className="tutorHead"
                src={require("../photo/tutorIMG.png")}
                alt="TutorIMG"
              />
            </div>
            <div className="profile experience">
              {" "}
              {/*experience in app.css*/}
              <div className="titleOnBorder">
                <b>Profile</b>
              </div>
              {/* if isEdit true it will change text to input box */}
              {this.state.isEdit ? (
                <div>
                  <label id="editFirstname">
                    First Name:
                    <br />
                    <p>
                      <input
                        type="text"
                        id="firstname"
                        value={this.state.firstname}
                        placeholder="Your firstname.."
                        onChange={e => {
                          this.handleChange(e);
                        }}
                      />
                    </p>
                  </label>
                  <label id="editLastname">
                    Last Name:
                    <br />
                    <input
                      type="text"
                      id="lastname"
                      value={this.state.lastname}
                      placeholder="Your lastname.."
                      onChange={e => {
                        this.handleChange(e);
                      }}
                    />
                  </label>
                  <label id="editGender">
                    Gender:
                    <br />
                    <select
                      id="gender"
                      value={this.state.gender}
                      onChange={e => {
                        this.handleChange(e);
                      }}
                    >
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                    </select>
                  </label>
                  <label id="editBirthyear">
                    Birth Year:
                    <br />
                    <input
                      type="number"
                      id="birthyear"
                      value={this.state.birthyear}
                      maxLength="4"
                      min="0"
                      onChange={e => {
                        this.handleChange(e);
                      }}
                    />
                  </label>
                  {!this.props.isStudent && (
                    <div>
                      <label id="editEmail">
                        Email:
                        <br />
                        <input
                          type="email"
                          id="email"
                          value={this.state.email}
                          placeholder="Your e-mail.."
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                      </label>
                      <label id="editBank">
                        Bank name:
                        <br />
                        <input
                          type="text"
                          id="bank"
                          value={this.state.bank}
                          placeholder="Your bank name.."
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                      </label>
                      <label id="editBanknumber">
                        Bank Number:
                        <br />
                        <input
                          type="number"
                          id="banknumber"
                          value={this.state.banknumber}
                          placeholder="Your back account number.."
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                      </label>
                      <label id="editPhonenumber">
                        Contact:
                        <br />
                        <input
                          type="text"
                          id="phonenumber"
                          value={this.state.phonenumber}
                          placeholder="Your phone number.."
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                      </label>
                    </div>
                  )}
                </div>
              ) : (
                <div>
                  <h5>First Name: {this.state.information["firstname"]}</h5>
                  <h5>Last Name: {this.state.information["lastname"]}</h5>
                  <h5>Gender: {this.state.information["gender"]}</h5>
                  <h5>Birth Year: {this.state.information["birthyear"]}</h5>
                  {!this.props.isStudent && (
                    <div>
                      <h5>Email: {this.state.information["email"]}</h5>
                      <h5>Bank: {this.state.information["bank"]}</h5>
                      <h5>
                        Bank Number: {this.state.information["banknumber"]}
                      </h5>
                      <h5>Contact: {this.state.information["phonenumber"]}</h5>
                    </div>
                  )}
                </div>
              )}
              <div className="profileBtn">
                {this.state.isEdit && (
                  <button
                    type="button"
                    class="btn btn-outline-primary btn-profile"
                    onClick={e => {
                      this.clickBack();
                    }}
                  >
                    Back
                  </button>
                )}
                {this.state.isEdit && (
                  <button
                    type="button"
                    class="btn btn-outline-primary btn-profile"
                    onClick={e => {
                      this.clickSave();
                    }}
                  >
                    Save Changes
                  </button>
                )}
                {!this.state.isEdit && (
                  <button
                    type="button"
                    class="btn btn-outline-danger btn-profile"
                    onClick={e => {
                      this.clickDeleteAccount();
                    }}
                  >
                    Delete Account
                  </button>
                )}
                {!this.state.isEdit && (
                  <button
                    type="button"
                    class="btn btn-outline-warning btn-profile"
                    onClick={e => {
                      this.clickEdit();
                    }}
                  >
                    Edit Profile
                  </button>
                )}
              </div>
            </div>{" "}
            {/*profile-div*/}
          </div>

          <div className="experienceContainer">
            <div className="titleOnBorder">
              <b>Experience</b>
            </div>
            {this.state.isEdit ? (
              <div>
                {this.props.isStudent && (
                  <label style={{ width: "100%" }}>
                    <textarea
                      style={{ width: "100%" }}
                      id="studyexp"
                      value={this.state.studyexp}
                      onChange={e => {
                        this.handleChange(e);
                      }}
                      rows={7}
                    />
                  </label>
                )}
                {!this.props.isStudent && (
                  <label style={{ width: "100%" }}>
                    <textarea
                      style={{ width: "100%" }}
                      id="exp"
                      value={this.state.exp}
                      onChange={e => {
                        this.handleChange(e);
                      }}
                      cols={30}
                      rows={7}
                    />
                  </label>
                )}
              </div>
            ) : (
              <div style={{ wordWrap: "break-word" }}>
                {this.props.isStudent && (
                  <h5 className="experience">
                    {this.state.information["study"]}
                  </h5>
                )}
                {!this.props.isStudent && (
                  <div>
                    <h5 className="experience">
                      {this.state.information["experience"]}
                    </h5>
                  </div>
                )}
              </div>
            )}
          </div>

          {/* See my course only tutor */}
          {!this.props.isStudent && (
            <div className="course-contain">
              <div className="titleOnBorder">
                <b>My course</b>
              </div>
              {this.props.tutorcourse.map((val, idx) => {
                return (
                  <div className="card-body">
                    <h5 className="card-title">{val.coursename}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                      {" "}
                      Subject :{" "}
                    </h6>
                    <h6 className="card-text">{val.subject}</h6>
                    <br />
                    <h6 className="card-subtitle mb-2 text-muted"> Price : </h6>
                    <h6 className="card-text">{val.price}</h6>
                    <br />
                    <h6 className="card-subtitle mb-2 text-muted">
                      {" "}
                      Address :{" "}
                    </h6>
                    <h6 className="card-text">{val.address}</h6>
                    <br />
                    <h6 className="card-subtitle mb-2 text-muted"> Time : </h6>
                    <h6 className="card-text">{val.time}</h6>
                    <br />
                    <h6 className="card-subtitle mb-2 text-muted">
                      {" "}
                      Course id :{" "}
                    </h6>
                    <h6 className="card-text">{val.courseid}</h6>
                    <br />

                    <div id="spacer" />
                    <button
                      id="delBtn"
                      className="btn btn-outline-danger"
                      onClick={e => {
                        if (
                          window.confirm(
                            `Are you sure to delete ${val.coursename}?`
                          )
                        ) {
                          this.props.database
                            .ref(`course/${val.courseid}`)
                            .remove();
                        }
                      }}
                    >
                      Delete
                    </button>
                  </div>
                );
              })}
            </div>
          )}
          <br />
        </div>
        <div className="go-top">
          <button
            id="topBtn"
            type="button"
            class="btn btn-primary"
            onClick={e => {
              window.scrollTo(0, 0);
            }}
          >
            Top
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isStudent: state.isStudent,
  database: state.database,
  userid: state.userid,
  tutorcourse: state.tutorcourse
});

const mapDispatchToProps = {
  getcourse,
  deleteAccount,
  //studentEdit(firstName, lastName, gender, birthYear, study)
  studentEdit,
  //tutorEdit(firstName, lastName, gender, birthYear, experience, bank, banknumber, email, phonenumber)
  tutorEdit
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
