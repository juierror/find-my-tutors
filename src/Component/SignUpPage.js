import React, { Component } from "react";
import { connect } from 'react-redux'
import { tutorSignup, studentSignup } from './../reducer'
import StudentSignUpPage from './StudentSignup'
import TutorSignUpPage from './TutorSignup'

class SignUpPage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isStudent:true
		};
		this.handleClickStudent = this.handleClickStudent.bind(this);
		this.handleClickTutor = this.handleClickTutor.bind(this);
	}


 	handleClickStudent() {
    	this.setState(state => ({
      		isStudent: true
    	}));
  	}

  	handleClickTutor() {
    	this.setState(state => ({
      		isStudent: false
    	}));
  	}

	render() {
		return (
			<div className="signUp-block">
				<h1>SignUp</h1>
				<button type="button" class={this.state.isStudent ? "btn btn-primary":"btn btn-secondary"} style={{marginRight: "5px"}} onClick={this.handleClickStudent} >Student</button>
				<button type="button" class={this.state.isStudent ? "btn btn-secondary":"btn btn-primary"} onClick={this.handleClickTutor} >Tutor</button>
				{this.state.isStudent ? <StudentSignUpPage /> : <TutorSignUpPage />}
			</div>
		)
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
	/*
	tutorSignup(idNumber, firstName, lastName, gender, birthYear, experience, bank, bankNumber, email, phonenumber, username, password)
	tutorSignup(idNumber, firstName, lastName, gender, studyExperience, username, password)
	*/
	tutorSignup,
	studentSignup,
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);
