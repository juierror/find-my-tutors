import React, { Component } from "react";
import TutorHeader from './TutorHeader';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import Profile from './Profile'
import TutorMycourse from './TutorMycourse'
import CreateCourse from './CreateCourse'
import Course from './Course'

class Tutor extends Component {
    constructor() {
        super()
    }
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <TutorHeader />
                        <Route path="/" exact component={Home} />
                        <Route path="/createcourse/" component={CreateCourse} />
                        <Route path="/profile/" component={Profile} />
                        <Route path="/course/" component={Course} />
                        <Route path="/tutorcourse/" component={TutorMycourse} />
                    </div>
                </Router>
            </div>
        );
    }
}

export default Tutor;
