import React, { Component } from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom'
import LoginPage from './LoginPage'
import SignUpPage from './SignUpPage'
import Home from './Home'
import Header from './Header'

class NotLogin extends Component {
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <Header />
                        <Route path="/" exact component={Home} />
                        <Route path="/login/" component={LoginPage} />
                        <Route path="/signup/" component={SignUpPage} />
                    </div>
                </Router>
            </div>
        );
    }
}

export default NotLogin;
