import React, { Component } from "react";
import { connect } from 'react-redux'
import { tutorSignup, studentSignup } from './../reducer'
import './StudentSignup.css';
class StudentSignUpPage extends Component {

	constructor(props) {
		//studentSignup(idNumber, firstName, lastName, gender, studyExperience, username, password)
		super(props);
		this.state = {
			firstname: "",
			lastname: "",
			username: "",
			password: "",
			gender: "male",
			birthyear: "",
			idcard: "",
			studyexp: ""
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	handleSubmit(event) {
		alert('Submit');
		event.preventDefault();
		this.props.studentSignup(this.state.idcard, this.state.firstname, this.state.lastname, this.state.gender, this.state.birthyear, this.state.studyexp, this.state.username, this.state.password);
	}

	render() {
		return (
			<div className="signUp-block">
				<form onSubmit={this.handleSubmit} className="signUp-form" style={{ borderRadius: "30px" }}>
					<label>
						ID card number:<br />
						<input type="number" id="idcard" required="required" value={this.state.idcard} min="0" minlength="10" maxlength="10" onChange={this.handleChange} placeholder="ID card number..." />
					</label>
					<label>
						Firstname:<br />
						<input type="text" id="firstname" required="required" value={this.state.firstname} onChange={this.handleChange} placeholder="Firstname" />
					</label>
					<label>
						Lastname:<br />
						<input type="text" id="lastname" required="required" value={this.state.lastname} onChange={this.handleChange} placeholder="Lastname" />
					</label>
					<label>
						Birth Year:<br />
						<input type="number" id="birthyear" maxlength="4" min="0" value={this.state.birthyear} onChange={this.handleChange} />
					</label>
					<label>
						Gender:<br />
						<select id="gender" value={this.state.gender} onChange={this.handleChange}>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
					</label>
					<label>
						Study Experience:<br />
						<input type="text" id="studyexp" value={this.state.studyexp} onChange={this.handleChange} placeholder="Study Experience" />
					</label>
					<label>
						Username:<br />
						<input type="text" id="username" required="required" value={this.state.username} onChange={this.handleChange} placeholder="Username" />
					</label>
					<label>
						Password:<br />
						<input type="password" id="password" required="required" value={this.state.password} onChange={this.handleChange} placeholder="Password" />
					</label>
					<button className="btn btn-outline-primary" type="submit">Sign Up</button>
				</form>
			</div>
		)
	}
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
	/*
	tutorSignup(idNumber, firstName, lastName, gender, birthYear, experience, bank, bankNumber, email, phonenumber, username, password)
	studentSignup(idNumber, firstName, lastName, birthYear, gender, studyExperience, username, password)
	*/
	tutorSignup,
	studentSignup,
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentSignUpPage);
