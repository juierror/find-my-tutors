import React, { Component } from "react";
import { connect } from "react-redux";
import {
  tutorReject,
  tutorAccept,
  tutorConfirmPayment,
  tutorNotConfirmPayment,
  tutorAlreadyTeach,
  updateTutorRequest,
  updateStudentRegister
} from "./../reducer";
import { NavLink } from "react-router-dom";
import "./TutorMycourse.css";
import "./Search.css";

class TutorMycourse extends Component {
  constructor() {
    super();
    this.state = {
      request: {}
    };
  }

  componentDidMount() {
    this.setState({ request: this.props.tutorrequest || {} });
  }

  stateToOb(state, courseid, studentid) {
    state = parseInt(state);
    if (state == 0) {
      return (
        <div>
          <button className='btn btn-outline-success'
            onClick={e => {
              this.props.tutorAccept(courseid, studentid);
              this.props.database
                .ref(`studentregister/`)
                .once("value", snap => {
                  //console.log(snap.val())
                  if (snap.val() != null)
                    this.props.updateStudentRegister(snap.val());
                });
            }}
          >
            Accept
          </button>
          <button className='btn btn-outline-danger btn-profile'
            onClick={e => {
              this.props.tutorReject(courseid, studentid);
              this.props.database.ref(`tutorrequest/`).once("value", snap => {
                console.log(snap.val());
                if (snap.val() != null)
                  this.props.updateTutorRequest(snap.val());
              });
            }}
          >
            Reject
          </button>
        </div>
      );
    } else if (state == 3) {
      return (
        <div>
          <h6>Student already pay for your course. Please confirm payment.</h6>
          <button className="btn btn-outline-info"
            onClick={e => {
              window.location.href = this.props.tutorrequest[courseid][studentid].image;
            }}>
            See Image
          </button>
          <button
            className="btn btn-outline-success"
            onClick={e => {
              this.props.tutorConfirmPayment(courseid, studentid);
              this.props.database
                .ref(`studentregister/`)
                .once("value", snap => {
                  console.log(snap.val());
                  if (snap.val() != null)
                    this.props.updateStudentRegister(snap.val());
                });
            }}
          >
            Confirm
          </button>
          <button
            className="btn btn-outline-danger"
            onClick={e => {
              this.props.tutorNotConfirmPayment(courseid, studentid);
              this.props.database
                .ref(`studentregister/`)
                .once("value", snap => {
                  console.log(snap.val());
                  if (snap.val() != null)
                    this.props.updateStudentRegister(snap.val());
                });
            }}
          >
            Not Recieve
          </button>
        </div>
      );
    } else if (state == 4) {
      return (
        <div>
          <button
            className="btn btn-outline-success"
            onClick={e => {
              this.props.tutorAlreadyTeach(courseid, studentid);
              this.props.database
                .ref(`studentregister/`)
                .once("value", snap => {
                  console.log(snap.val());
                  if (snap.val() != null)
                    this.props.updateStudentRegister(snap.val());
                });
            }}
          >
            Already Teach
          </button>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="background01">
        <div className="tutorCourse-page">
          <div className="course-contain">
            <div className="titleOnBorder">
              <b>Course status</b>
            </div>
            {Object.keys(this.state.request).map(courseid => {
              if (courseid.startsWith(this.props.userid)) {
                return (
                  <div className="card-body">
                    <h6 className="card-title">
                      Course Name: {this.props.allCourse[courseid].coursename}
                    </h6>
                    <h6 className="card-title">
                      Course Info:{" "}
                      <NavLink
                        to={{ pathname: "/course", search: `?id=${courseid}` }}
                      >
                        Info
                      </NavLink>
                    </h6>
                    {Object.keys(this.state.request[courseid]).map(
                      studentid => {
                        return (
                          <div>
                            <h6 className="card-title">
                              StudentID: {studentid}
                            </h6>
                            <h6 className="card-title">
                              Request Address:{" "}
                              {
                                this.state.request[courseid][studentid]
                                  .address
                              }
                            </h6>
                            <h6 className="card-title">
                              Request Time:{" "}
                              {this.state.request[courseid][studentid].time}
                            </h6>
                            <h6 className="card-title">
                              state:{" "}
                              {this.state.request[courseid][studentid].state}
                            </h6>
                            {this.stateToOb(
                              this.state.request[courseid][studentid].state,
                              courseid,
                              studentid
                            )}
                          </div>
                        );
                      }
                    )}
                  </div>
                );
              }
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  registerInformation: state.registerInformation,
  userid: state.userid,
  tutorrequest: state.tutorrequest,
  allCourse: state.allCourse,
  database: state.database
});

const mapDispatchToProps = {
  //state 1
  tutorReject,
  //state 2
  tutorAccept,
  //state 4
  tutorConfirmPayment,
  //state 5
  tutorNotConfirmPayment,
  //state 6
  tutorAlreadyTeach,
  updateTutorRequest,
  updateStudentRegister
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TutorMycourse);
