import React, { Component } from "react";
import { Jumbotron, Button,Container,Row,Col } from 'reactstrap';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import './Home.css'
class Home extends Component {
    render() {
        return (
            <div id = 'Home'>
                <link href="https://fonts.googleapis.com/css?family=Kanit&amp;subset=thai" rel="stylesheet"></link>
                <Jumbotron id='Head'>
                    <h1 className="title">Find My Tutor</h1>
                    <p className="lead1">เว็บหาติวเตอร์อันดับหนึ่ง.</p>
                    <p className="lead1">ทุกระดับชั้น ตั้งแต่ มัธยม จนถึง มหาวิทยาลัย</p>
                    {
                        this.props.isLogin? null :
                        <NavLink to = "/signup">
                            <Button color="primary" size='lg'>สมัครเลย</Button>
                        </NavLink>
                    }
                  
                 </Jumbotron>  
                 
                 <Row>
                     <Col md={{ size: 3, offset: 0 }}>
                            <img src='https://image.flaticon.com/icons/svg/906/906175.svg'  alt='Tutor'></img>
                                <div className='desc'>สอนสดแบบตัวต่อตัว</div>
                                <div className='desc'> หรือแบบกลุ่มเล็กๆ</div>
                    </Col>
                    <Col md={{ size: 3, offset: 0 }}>
                        <img src='https://image.flaticon.com/icons/svg/138/138773.svg' alt='Calendar'></img>
                            <div className='desc'>เรียนที่ไหนก็ได้</div>
                            <div className='desc'> เลือก วัน เวลา ได้ตามสะดวก</div>
                    </Col>
                    <Col md={{ size: 3, offset: 0 }}>
                        <img src='https://image.flaticon.com/icons/svg/201/201651.svg' alt='Calendar'></img>
                            <div className='desc'>ติวเข้มพิชิตโจทย์หลากหลาย</div>
                            <div className='desc'>ด้วยเทคนิคใหม่ๆเพียบ</div>
                    </Col>
                    <Col md={{ size: 3, offset: 0 }} id="lastCol">
                        <img src='https://image.flaticon.com/icons/svg/639/639365.svg' alt='Calendar'></img>
                            <div className='desc'>ราคาที่คุณเลือกได้</div>
                            <div className='desc'></div>
                    </Col>
                   </Row>
                   
            </div>
        )
    }
}
const mapStateToProps = state => ({
  isLogin: state.isLogin,
})
export default connect(mapStateToProps)(Home);
