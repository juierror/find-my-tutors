import React, { Component } from "react";
import { createCourse } from './../reducer'
import { connect } from 'react-redux'
import './CreateCourse.css'
class CreateCourse extends Component {
    constructor() {
        super()
        this.state = {
            subject: "Computer",
            cname: "",
            price: 300,
            address: "",
            time: "",
            subject_list: [
                "Computer",
                "Physics",
                "Math",
                "Chemistry",
                "Science",
                "Biology",
                "Thai",
                "Social Studies",
                "English",
                "Japan",
                "German",
                "Russia",
                "Other"
            ]
        }
    }
    render() {
        return (
            <div class='CreateCourse-block'>
                <form onSubmit={e => {
                    this.props.createCourse(this.state.address, this.state.cname, this.state.price, this.state.subject, this.state.time)
                }} className='CreateCourse-form'>

                    <label> Course Name: <br />
                        <input type="text" value={this.state.cname} onChange={e => this.setState({ cname: e.target.value })} placeholder="Ex. Data Sci" />
                    </label>

                    <label>Price per hour: <br />
                        <input type="number" max="1000" min="0" value={this.state.price} onChange={e => this.setState({ price: e.target.value })} />
                    </label>
                    <label>Subject: <br />
                        <select
                            value={this.state.subject}
                            onChange={e => this.setState({ subject: e.target.value })}
                        >
                            {this.state.subject_list.map((val, idx) => {
                                return <option value={val}>{val}</option>;
                            })}
                        </select>
                    </label>
                    <label>Address: <br />
                        <input type="text" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} />
                    </label>
                    <label>Time: <br />
                        <input type="text" value={this.state.time} onChange={e => this.setState({ time: e.target.value })} />
                    </label>
                    <button type="submit" class="btn btn-outline-success">Submit</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = {
    createCourse
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateCourse);
