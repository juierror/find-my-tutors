import React, { Component } from "react";
import StudentHeader from './StudentHeader';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import Search from './Search'
import Profile from './Profile'
import StudentMycourse from './StudentMycourse'
import Register from './Register'
import Course from './Course'

class Student extends Component {
    constructor() {
        super()
    }
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <StudentHeader />
                        <Route path="/" exact component={Home} />
                        <Route path="/search/" component={Search} />
                        <Route path="/profile/" component={Profile} />
                        <Route path="/studentcourse/" component={StudentMycourse} />
                        <Route path="/course/" component={Course} />
                        <Route path="/register/" component={Register} />
                    </div>
                </Router>
            </div>
        );
    }
}

export default Student;
